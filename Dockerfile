from  node:8
workdir /usr/src/app
copy package*.json ./
run npm install
COPY . .
EXPOSE 3020
CMD [ "npm","start" ]
